const { SlashCommandBuilder } = require('discord.js');
const axios = require('axios');
const querystring = require('querystring');
const { urljenkins,tokenjenkins } = require('../../config.json');

// Variabel-variable untuk mengirim request HTTP ke jenkins
const url = `https://${urljenkins}/job/test-discord/buildWithParameters`;
const urlinfo = `https://${urljenkins}/job/test-discord/api/json`;
const authHeaderValue = tokenjenkins;
const config = {
	headers: {
	  Authorization: authHeaderValue,
	  'Content-Type': 'application/x-www-form-urlencoded'
	}
  };
const opsiWaktu = { 
	year: 'numeric', 
	month: 'numeric', 
	day: 'numeric', 
	hour: 'numeric', 
	minute: 'numeric', 
	second: 'numeric' 
};
const currentDate = new Date();
const maxRetries = 5; // Misalnya, coba 5 kali


async function retryRequestWithLimit(url, maxRetries, config) {
for (let retry = 0; retry <= maxRetries; retry++) {
	try {
	const response = await axios.get(url, config);

	if (response.status === 200) {
		return response.data;
	}
	} catch (error) {
	if (retry < maxRetries) {
		console.log(`Retry ${retry + 1} - Error: ${error.message}`);
		await new Promise(resolve => setTimeout(resolve, 5000)); // Menunggu 1 detik sebelum mencoba lagi
	} else {
		throw new Error(`Max retries reached: ${error.message}`);
	}
	}
}
}

module.exports = {
	data: new SlashCommandBuilder()
		.setName('buildandroid')
		.setDescription('Akan melakukan build jenkins, dan reply onprogress')
		.addStringOption(option => option.setName('namabranch').setDescription('Input nama repo gitlabnya yaa').setRequired(true))
		.addStringOption(option =>
			option.setName('jenisbuild')
			.setDescription('Ingin build tipe aplikasinya apa')
			.setRequired(true)
			.addChoices(
				{ name: 'Preprod Debug', value: 'debugpreprod' },
				{ name: 'Preprod Release', value: 'releasepreprod' },
			))
			.addStringOption(option =>
				option.setName('environment')
				.setDescription('Ingin build apps environment apa')
				.setRequired(true)
				.addChoices(
					{ name: 'preprod', value: 'preprod' },
					{ name: 'production', value: 'production' },
				)),
	async execute(interaction) {
		const GIT_BRANCH_NAME = interaction.options.getString('namabranch');
		const BUILD_FOR = interaction.options.getString('jenisbuild');
		const ENV = interaction.options.getString('environment');
		const formData = querystring.stringify({ GIT_BRANCH_NAME: GIT_BRANCH_NAME, BUILD_FOR:BUILD_FOR,ENV:ENV });
		// const buildNumber = 0;
		let estimation = 0;
		
		console.log('sending data to Jenkins')
		axios.post(url, formData, config)
		.then(response => {
			console.log(response.data);
		})
		.catch(error => {
			console.error(`Error: ${error.message}`);
		});

		axios.get(urlinfo, config)
		.then(response => {
			const buildNumber = response.data.lastBuild.number + 1;
			console.log(buildNumber);
			if(buildNumber!=0){
				const urlbuild = `https://${urljenkins}/job/test-discord/`+buildNumber+`/api/json`
				retryRequestWithLimit(urlbuild, maxRetries, config)
				.then(async data => {
					console.log(data.estimatedDuration);
					responseDuration=data.estimatedDuration;
					estimation = Math.round(responseDuration / 60 / 1000);
					const newDate = new Date(currentDate.getTime() + estimation * 60 * 1000).toLocaleString('id-ID', opsiWaktu); // Menambah 90 menit (90 * 60 * 1000 milidetik)
					const formattedText = `Job mu sudah mulai jalan nih.... \n-----------------\nBuild Apps : **ANDROID** \nBranch : **${GIT_BRANCH_NAME}** \nEstimasi : **${estimation} menit (${newDate})**\nNomor Build : ${buildNumber}\n---------------\n Silahkan ketik command /checkstatus dan inputkan Nomor Build untuk mengecek status runningnya`;
					
					await interaction.followUp({ content: formattedText, ephemeral: true });
				})
				.catch(error => {
					console.error('Final Error:', error.message);
				});
				const formattedText2 = `On Progress yaa.... lagi memulai job build ${buildNumber} nih, nanti kakak update lagi. Stay Tune!\n`;
					
				
				return interaction.reply({ content: formattedText2, ephemeral: true });
			}
		})
		.catch(error => {
			console.error(`Error: ${error.message}`);
		});
	},
};

