const { SlashCommandBuilder } = require('discord.js');
const axios = require('axios');
const { tokenjira, jiraurl, jiraspacekey } = require('../../config.json');


const maxRetries = 5; // Misalnya, coba 5 kali
const authHeaderValue = tokenjira;
const config = {
	headers: {
	  Authorization: authHeaderValue,
	  'Content-Type': 'application/json'
	}
  };

async function retryRequestWithLimit(url, maxRetries, config) {
for (let retry = 0; retry <= maxRetries; retry++) {
	try {
	const response = await axios.get(url, config);

	if (response.status === 200) {
		return response.data;
	}
	} catch (error) {
	if (retry < maxRetries) {
		console.log(`Retry ${retry + 1} - Error: ${error.message}`);
		await new Promise(resolve => setTimeout(resolve, 5000)); // Menunggu 1 detik sebelum mencoba lagi
	} else {
		throw new Error(`Max retries reached: ${error.message}`);
	}
	}
}
}


module.exports = {
	data: new SlashCommandBuilder()
		.setName('carisop')
		.setDescription('Akan melakukan pencarian page sesuai kategori, di JIRA')
		.addStringOption(option =>
			option.setName('category')
			.setDescription('ingin cari by title atau all')
			.setRequired(true)
			.addChoices(
				{ name: 'SOP', value: 'SOP' },
				{ name: 'MOP Deployment', value: 'MOP' },
			))
		.addStringOption(option => option.setName('keyword').setDescription('Input keyword SOP nya').setRequired(true))		
		.addStringOption(option =>
			option.setName('filter')
			.setDescription('ingin cari by title atau all')
			.setRequired(true)
			.addChoices(
				{ name: 'Title', value: 'title' },
				{ name: 'All', value: 'text' },
			)),
	async execute(interaction) {
		// Mengambil value yang diinputkan
		const keyword = interaction.options.getString('keyword');
		const category = interaction.options.getString('category');
		const filter = interaction.options.getString('filter');
		let title = false
		if(filter=='title'){
			title='&title=true'
		}
        const url = `${jiraurl}/wiki/rest/api/content/search?cql=space.key=${jiraspacekey}+AND+${filter}~"${category} ${keyword}"+AND+type="page"`;
        const formattedText2 = `Wait lagi mencari ${category} ${keyword}\n`;
            
        return interaction.reply({ content: formattedText2, ephemeral: true }).then(msg => 
			retryRequestWithLimit(url, maxRetries, config)
            .then(data => {
				let daftartitle = ""
				let nomor = 0
				data.results.forEach(dataconfluence => {
					nomor++;
					if(nomor<16){
						const pageTitle = dataconfluence.title;
						const tinyui = dataconfluence._links.tinyui;
						daftartitle += `${nomor}. ${pageTitle} (${jiraurl}/wiki${tinyui})\n`
					}
				  });
				  if(nomor==0){
					daftartitle+=`\n----------------\nData ${category} Tidak ditemukan, silahkan coba keyword lain\n----------------\n.\n`
				  }else{
					daftartitle+=`\n----------------\nSearch more results on ${jiraurl}/search?text=${keyword}\n----------------\n.\n`
				  }
				  // await interaction.deferReply();
					
                // return interaction.followUp({content: formattedText, ephemeral:true});
                // return interaction.followUp({content:daftartitle, ephemeral:true});
                return interaction.followUp({content:daftartitle, ephemeral:true});
				// msg.edit(nyoba);
            })
            .catch(error => {
                console.error('Final Error:', error.message);
            })
		);
    }
};